`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Design Name: Dual Port SRAM 
// Module Name: 
//
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.05 
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`ifndef AXI_ADDR_WIDTH
	`define AXI_ADDR_WIDTH 20
`endif
`ifndef AXI_DATA_WIDTH
	`define AXI_DATA_WIDTH 32
`endif
`ifndef STRB_SIZE
	`define STRB_SIZE (`AXI_DATA_WIDTH/8)
`endif
`ifndef RAM_DEPTH
	`define RAM_DEPTH 16 //2^16 = 64KB
`endif
`ifndef
	`define RAM_ADDR_WIDTH (`RAM_DEPTH - $clog2(`AXI_DATA_WIDTH/8))
`endif

module dpram_sclk
(
	input [`AXI_DATA_WIDTH-1:0]      wr_data,
	input [`AXI_ADDR_WIDTH-1:0]      wr_addr,
	input                            we,
	input [`STRB_SIZE-1:0]           be,
	input                            clk,
	input [`AXI_ADDR_WIDTH-1:0]      rd_addr,
	input                            rd_en,
	output reg [`AXI_DATA_WIDTH-1:0] rd_data
);

parameter RAM_SIZE = (1<<`RAM_ADDR_WIDTH);
	// Declare the RAM variable
	reg [7:0] ram [`STRB_SIZE-1:0] [RAM_SIZE-1:0];
	reg [7:0] temp [`STRB_SIZE-1:0];

genvar i, j;
generate
for(i=0; i<`STRB_SIZE; i++) begin
always_latch
begin
	temp[i] = wr_data[(8*i)+7:(8*i)];
end
end
endgenerate

	// Port A
	always @ (posedge clk)
	begin
		if (we)
		begin
			//temp = ram[wr_addr];
			for(integer c=0; c<`STRB_SIZE; c=c+1) begin: write_model
				//temp[c] = ram [c] [wr_addr];
          if(be[c]==1)
					  //temp[c] = wr_data[(8*c)+7:(8*c)];
					  ram[c] [wr_addr] = temp[c];
			end
			//ram[wr_addr] = temp;
		end
	end

	// Port B
generate
for(i=0; i<`STRB_SIZE; i++) begin
	always @ (posedge clk)
	begin
	  if(rd_en)
		  rd_data[(8*i)+7:(8*i)] <= ram [i] [rd_addr];
	end
end
endgenerate

integer di,dj;
initial begin
  for(di=0;di<`STRB_SIZE;di++) begin
    for(dj=0;dj<RAM_SIZE;dj++) begin
      ram[di][dj] = $urandom_range(8'h00, 8'hff);
    end
  end
end

endmodule

