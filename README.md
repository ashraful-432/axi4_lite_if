# AXI4_LITE_IF

This repository contains AXI4 Lite Interface wrapper for dual port SRAM.

* src/axi4_lite_sram_if.sv (contains wrapper module axi4_lite_sram_if)

* tests/test.sv (contains simple testcase to read and write to SRAM through AXI4 Lite interface)

* models/dpSRAM_model.sv (contaitns Dual-Port SRAM model which is instantiated in axi4_lite_sram_if module)


