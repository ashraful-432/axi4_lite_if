//Put the timescale same as SRAM model
`timescale 1 ns/1 ps

`define AXI_ADDR_WIDTH 20
`define AXI_DATA_WIDTH 32
`define STRB_SIZE (`AXI_DATA_WIDTH/8)
`define RAM_DEPTH 13 //2^13 = 8K Thus capacity => 8k * 4B (32-bit data width) = 32KB
//`define RAM_ADDR_WIDTH (`RAM_DEPTH - $clog2(`AXI_DATA_WIDTH/8))
`define RAM_DATA_WIDTH 32

module tb(); //AWPROT,ARPROT

logic                           s_axi_clk;
logic                           s_axi_rst_n;
logic [`AXI_ADDR_WIDTH - 1 : 0] s_axi_awaddr;
logic [`AXI_DATA_WIDTH - 1 : 0] s_axi_wdata;
logic [`AXI_DATA_WIDTH - 1 : 0] test_wr_data;
logic                           s_axi_awvalid;
logic                           s_axi_awready;
logic                           s_axi_wvalid;
logic                           s_axi_wready;
logic                           s_axi_bvalid;
logic                           s_axi_bready;
logic [                  1 : 0] s_axi_bresp;
logic [`STRB_SIZE -      1 : 0] s_axi_wstrb;

logic [`AXI_ADDR_WIDTH - 1 : 0] s_axi_araddr;
logic [`AXI_DATA_WIDTH - 1 : 0] s_axi_rdata;
logic [`AXI_DATA_WIDTH - 1 : 0] test_rd_data;
logic                           s_axi_arvalid;
logic                           s_axi_arready;
logic                           s_axi_rvalid;
logic                           s_axi_rready;
logic [                  1 : 0] s_axi_rresp;

always #7 s_axi_clk = ~s_axi_clk;

task automatic axi_read;
  input [`AXI_ADDR_WIDTH - 1 : 0] addr;
  input [`AXI_DATA_WIDTH - 1 : 0] expected_data;
  begin
    s_axi_araddr = addr;
    s_axi_arvalid = 1;
    s_axi_rready = 1;
    wait(s_axi_arready);
    @(posedge s_axi_clk) #2;
    s_axi_arvalid = 0;

    wait(s_axi_rvalid);
    if (s_axi_rdata != expected_data) begin
      $display("Error: Mismatch in AXI4 read at %x: ", addr,
        "expected %x, received %x",
        expected_data, s_axi_rdata);
    end
    if(s_axi_rresp != 2'b00) begin
	    $display("Error: Read response return non zero values (%x) ", s_axi_rresp);
    end
    @(posedge s_axi_clk) #2;    
    s_axi_rready = 0;

  end
endtask

task automatic axi_write;
  input [`AXI_ADDR_WIDTH - 1 : 0] addr;
  input [`AXI_DATA_WIDTH - 1 : 0] data;
  begin
    s_axi_wdata = data;
    s_axi_awaddr = addr;
    s_axi_awvalid = 1;
    s_axi_wvalid = 1;
    wait(s_axi_awready);
    @(posedge s_axi_clk) #2;
    s_axi_awvalid = 0;

    wait(s_axi_wready);
    @(posedge s_axi_clk) #2; 
    s_axi_wvalid = 0;

    wait(s_axi_bvalid)
    @(posedge s_axi_clk) #2;
    s_axi_bready = 1;
    if(s_axi_bresp != 2'b00) begin
	    $display("Error: Write response return non zero values (%x) ", s_axi_bresp);
    end
    @(posedge s_axi_clk) #2;
    s_axi_bready = 0;

  end
endtask


initial
begin
	s_axi_clk = 0;
	s_axi_rst_n = 0;

	s_axi_awaddr = 0;
	s_axi_awvalid = 0;
	s_axi_wdata = 0;
	s_axi_wvalid = 0;
	s_axi_bready = 0;
	s_axi_wstrb = {`STRB_SIZE{1'b1}};

	s_axi_araddr = 0;
	s_axi_arvalid = 0;
	s_axi_rready = 0;

	test_wr_data = 32'h00FF_00FF;
	test_rd_data = test_wr_data;
	#15;
	s_axi_rst_n = 1;
	@(negedge s_axi_clk) #3;
	axi_write(0, test_wr_data);
	#15;
	@(negedge s_axi_clk) #3;
	axi_write(1'b1<<13, test_wr_data);

	@(negedge s_axi_clk) #3;
	axi_read(1'b1<<13, test_wr_data);
	@(negedge s_axi_clk) #3;
	axi_read(0, test_wr_data);
	#15;
	$stop;
end

axi2mem axi2mem_inst(
	.ACLK(s_axi_clk),
	.ARESETn(s_axi_rst_n),
	.AWVALID(s_axi_awvalid),
	.AWREADY(s_axi_awready),
	.AWADDR(s_axi_awaddr), 
	.WVALID(s_axi_wvalid),
	.WREADY(s_axi_wready),
	.WDATA(s_axi_wdata),
	.BVALID(s_axi_bvalid),
	.BREADY(s_axi_bready),
	.BRESP(s_axi_bresp), 
	.ARVALID(s_axi_arvalid),
	.ARREADY(s_axi_arready),
	.ARADDR(s_axi_araddr),
	.RVALID(s_axi_rvalid),
	.RREADY(s_axi_rready),
	.RDATA(s_axi_rdata),
	.RRESP(s_axi_rresp),
	.WSTRB(s_axi_wstrb)
);

endmodule
