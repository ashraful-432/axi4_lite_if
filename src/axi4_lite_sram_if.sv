`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Design Name: 
// Module Name: axi4_lite_sram_if
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.05 
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`ifndef AXI_ADDR_WIDTH
	`define AXI_ADDR_WIDTH 20
`endif
`ifndef AXI_DATA_WIDTH
	`define AXI_DATA_WIDTH 32
`endif
`define STRB_SIZE (`AXI_DATA_WIDTH/8)
`define RAM_DEPTH 16 //2^16 = 64KB
`define RAM_ADDR_WIDTH (`RAM_DEPTH - $clog2(`AXI_DATA_WIDTH/8))

module axi4_lite_sram_if(ACLK,ARESETn,AWVALID,AWREADY,AWADDR, WVALID,WREADY,WDATA,BVALID,BREADY,BRESP, ARVALID,ARREADY,ARADDR,RVALID,RREADY,RDATA,RRESP,WSTRB);

parameter IDLE_W      = 2'b00;
parameter WR_WDATA    = 2'b01;
parameter WAIT_4_RESP = 2'b10;

reg      [1:0]  pstate_w;
reg      [1:0]  nstate_w;

parameter IDLE_AW    = 2'b00;
parameter WR_WRDATA  = 2'b01;
parameter WR_RAM     = 2'b10;
parameter RESP_AW    = 2'b11;

reg	[1:0]	pstate_aw;
reg	[1:0]	nstate_aw;

parameter IDLE_RD    = 2'b00;
parameter READ_RD  = 2'b01;
parameter RESP_RD    = 2'b10;

reg 	[1:0]	pstate_rd;
reg	  [1:0]	nstate_rd;

//Input Output signals
input 		ACLK;
input 		ARESETn;
//signal of Write address channel
input 				AWVALID;
output 				AWREADY;
input 	[`AXI_ADDR_WIDTH-1:0] 	AWADDR;
//input 		AWPROT;
//signal of write data channel
input 				WVALID;
output 				WREADY;
input 	[`AXI_DATA_WIDTH-1:0]	WDATA;
input 	[`STRB_SIZE-1:0]	WSTRB;
//signal of write response channel
output 				BVALID;
input 				BREADY;
output 	[1:0]			BRESP;
// signal of read address channel
input 				ARVALID;
output 				ARREADY;
input 	[`AXI_ADDR_WIDTH-1:0]	ARADDR;
//input 		ARPROT;
//signal of read data channel
output 				RVALID;
input 				RREADY;
output 	[`AXI_DATA_WIDTH-1:0]	RDATA;
output 	[1:0]			RRESP;

reg				WREADY;
reg				AWREADY;
reg				BVALID;
reg	[1:0]			BRESP=2'b00;
reg				ARREADY;
reg				RVALID;
reg	[1:0]			RRESP=2'b00;
reg	[`AXI_DATA_WIDTH-1:0]	RDATA;

wire 				wr_complete;
reg 				wr_data_cap; // Signal to indicate that write data has been captured
reg 				wr_addr_cap; // Signal to indicate that write addr has been captured
reg 				wr_ack;
reg [`AXI_ADDR_WIDTH-1:0] 	AWADDR_r;
reg [`AXI_DATA_WIDTH-1:0] 	WDATA_r;
reg [`STRB_SIZE-1:0]		WSTRB_r;
wire 				RAM_wr_req;
reg 				rd_ack;
reg [`AXI_ADDR_WIDTH-1:0]	ARADDR_r;
wire [`AXI_DATA_WIDTH-1:0]	RDATA_ram;
wire 				RAM_rd_req;


assign wr_complete = (pstate_aw == RESP_AW) && BREADY;
assign RAM_wr_req = (pstate_aw == WR_RAM); //&& !wr_ack;
assign RAM_rd_req = (pstate_rd == READ_RD); //&& !rd_ack;

always_ff @(posedge ACLK or negedge ARESETn )
begin
	if(!ARESETn)
	begin
		pstate_w <= IDLE_W;
		pstate_aw <= IDLE_AW;
		pstate_rd <= IDLE_RD;

	end
	else
	begin
		pstate_w <= nstate_w;
		pstate_aw <= nstate_aw;
		pstate_rd <= nstate_rd;
	end
end

always_comb// @(*)
begin
	unique case (pstate_w)
		IDLE_W:
		begin
			#1 WREADY = 1'b0;
			 wr_data_cap =1'b0;
			if(wr_addr_cap)
			begin
				nstate_w = WR_WDATA;
			end
			else
			begin
				nstate_w = IDLE_W;
			end
		end
		WR_WDATA:
		begin
			#1 WREADY = 1'b1;
			 wr_data_cap =1'b0;
			if(WVALID)
			begin
				nstate_w = WAIT_4_RESP;
			end
			else
			begin
				nstate_w = WR_WDATA;
			end
		end
		WAIT_4_RESP:
		begin
		  #1 WREADY = 1'b0;
		   wr_data_cap =1'b1;
			if(wr_ack)
			begin
			  nstate_w = IDLE_W;
			end
                        else //zhangjy add
			begin
			  nstate_w = WAIT_4_RESP;
			end
		end
		default:
		begin
		   #1 WREADY = 1'b0;
		    wr_data_cap =1'b0;
		   nstate_w = IDLE_W;
		end
	endcase
end

always_comb //@(*)
begin
	unique case (pstate_aw)
		IDLE_AW:
		begin
			#1 AWREADY = 1'b1;
			 BVALID = 1'b0;
			//BRESP = 2'b00;
			 wr_addr_cap = 1'b0;
			if(AWVALID)
			begin
				nstate_aw = WR_WRDATA;
			end
			else
			begin
				nstate_aw = IDLE_AW;
			end
		end
		WR_WRDATA:
		begin
			#1 AWREADY = 1'b0;
			 BVALID = 1'b0;
			//BRESP = 2'b00;
			 wr_addr_cap = 1'b1;
			if(wr_data_cap)
			begin
				nstate_aw = WR_RAM;
			end
			else
			begin
				nstate_aw = WR_WRDATA;
			end
		end
		WR_RAM:
		begin
			#1 AWREADY = 1'b0;
			 BVALID = 1'b0;
			//BRESP = 2'b00;
			 wr_addr_cap = 1'b0;
			if(wr_ack)
			begin
				nstate_aw = RESP_AW;
			end
			else
			begin
				nstate_aw = WR_RAM ;
			end
		end
		RESP_AW:
		begin
			#1 AWREADY = 1'b0;
			 BVALID = 1'b1;
			//BRESP = 2'b00;
			 wr_addr_cap = 1'b0;
			if(BREADY)
			begin
				nstate_aw = IDLE_AW;
			end
			else
			begin
				nstate_aw = RESP_AW ;
			end
		end
		default:
		begin
			 AWREADY = 1'b1;
			 BVALID = 1'b0;
			 wr_addr_cap = 1'b0;
			 nstate_aw = IDLE_AW;
		end
	endcase
end

always_ff @(posedge ACLK or negedge ARESETn )
begin
	if (!ARESETn)
	begin
		AWADDR_r <= `AXI_ADDR_WIDTH'd0;
		WDATA_r <= `AXI_DATA_WIDTH'd0;
		WSTRB_r <= 0;
	end
	else
	begin
		if((AWREADY == 1'b1) && (AWVALID ==1'b1))
		begin
			AWADDR_r <= AWADDR;
		end
		if ((WREADY == 1'b1) && (WVALID ==1'b1))
		begin
			WDATA_r <= WDATA;
			WSTRB_r <= WSTRB;
		end
	end
end

always_comb //@(*)
begin
	unique case (pstate_rd)
		IDLE_RD:
		begin
			#1 ARREADY = 1'b1;
			RVALID = 1'b0;
			//RRESP = 2'b00;
			if(ARVALID)
			begin
				nstate_rd = READ_RD;
			end
			else
			begin
				nstate_rd = IDLE_RD;
			end
		end
		READ_RD:
		begin
			#1 ARREADY = 1'b0;
			RVALID = 1'b0;
			//RRESP = 2'b00;
			if(rd_ack)
			begin
				nstate_rd = RESP_RD;
			end
			else
			begin
				nstate_rd = READ_RD;
			end
		end
		RESP_RD:
		begin
			#1 ARREADY = 1'b0;
			RVALID = 1'b1;
			//RRESP = 2'b00;
			if(RREADY)
			begin
				nstate_rd = IDLE_RD;
			end
			else
			begin
				nstate_rd = RESP_RD;
			end
		end
		default:
		begin
			ARREADY = 1'b1;
			RVALID = 1'b0;
			nstate_rd = IDLE_RD;
		end
	endcase
end

always_ff @(posedge ACLK or negedge ARESETn )
begin
	if (!ARESETn)
	begin
		ARADDR_r <= `AXI_ADDR_WIDTH'd0;
		RDATA <= `AXI_DATA_WIDTH'd0;
	end
	else
	begin
		if((ARREADY == 1'b1) && (ARVALID ==1'b1))
		begin
			ARADDR_r <= ARADDR;
		end
		if (rd_ack)
		begin
			RDATA <= RDATA_ram;
		end
	end
end

//---------------------------------
// Address Decoder
//---------------------------------
wire                       SRAM_wr_req;
wire                       SRAM_wr_ack;
wire [`AXI_ADDR_WIDTH-1:0] SRAM_wr_addr;
reg                        SRAM_rd_req;
wire                       SRAM_rd_ack;
reg  [`AXI_ADDR_WIDTH-1:0] SRAM_rd_addr;

assign SRAM_wr_req = RAM_wr_req;
assign SRAM_wr_addr = AWADDR_r;

always_latch
begin
   if(RAM_rd_req)
	 begin
	    if(AWADDR_r==ARADDR_r)
			begin
			   if(RAM_wr_req==1'b1)
				 begin
				    SRAM_rd_req = 1'b0;
					SRAM_rd_addr = SRAM_rd_addr;
				 end
				 else
				 begin
				    SRAM_rd_req = RAM_rd_req;
					SRAM_rd_addr = ARADDR_r;
				 end
			end
	    else
			begin
			   SRAM_rd_req = RAM_rd_req;
				 SRAM_rd_addr = ARADDR_r;
			end
	 end
	 else
	 begin
	    SRAM_rd_req = 1'b0;
		SRAM_rd_addr = SRAM_rd_addr;
	 end
end

// Internally generate rd_ack and wr_ack since RD/WR action performed in 1 clock cycle
always @(posedge ACLK or ARESETn )
begin
	if (!ARESETn)
	begin
		wr_ack <= 1'b0;
		rd_ack <= 1'b0;
	end
	else
	begin
	  wr_ack <= SRAM_wr_req;
		//rd_ack <= SRAM_rd_ack;
		rd_ack <= SRAM_rd_req; //modified
	end
end
// End Address Decoder

dpram_sclk dpram_sclk
(
	.wr_data(WDATA_r),
	.wr_addr(SRAM_wr_addr),
	.we(SRAM_wr_req),
	.be(WSTRB_r),
	.clk(ACLK),
	.rd_addr(SRAM_rd_addr),
	.rd_en(SRAM_rd_req),
	.rd_data(RDATA_ram)
);

endmodule


module dpram_sclk
(
	input [`AXI_DATA_WIDTH-1:0]      wr_data,
	input [`AXI_ADDR_WIDTH-1:0]      wr_addr,
	input                            we,
	input [`STRB_SIZE-1:0]           be,
	input                            clk,
	input [`AXI_ADDR_WIDTH-1:0]      rd_addr,
	input                            rd_en,
	output reg [`AXI_DATA_WIDTH-1:0] rd_data
);

parameter RAM_SIZE = (1<<`RAM_ADDR_WIDTH);
	// Declare the RAM variable
genvar i, j;
//generate
//for(i=0; i<`STRB_SIZE; i++) begin
	reg [7:0] ram [`STRB_SIZE-1:0] [RAM_SIZE-1:0];
	reg [7:0] temp [`STRB_SIZE-1:0];
//end
//endgenerate
	//reg [`AXI_DATA_WIDTH-1:0] temp;
generate
for(i=0; i<`STRB_SIZE; i++) begin
always_latch
begin
	temp[i] = wr_data[(8*i)+7:(8*i)];
end
end
endgenerate

	// Port A
	always @ (posedge clk)
	begin
		if (we)
		begin
			//temp = ram[wr_addr];
			for(integer c=0; c<`STRB_SIZE; c=c+1) begin: write_model
				//temp[c] = ram [c] [wr_addr];
          if(be[c]==1)
					  //temp[c] = wr_data[(8*c)+7:(8*c)];
					  ram[c] [wr_addr] = temp[c];
			end
			//ram[wr_addr] = temp;
		end
	end

	// Port B
generate
for(i=0; i<`STRB_SIZE; i++) begin
	always @ (posedge clk)
	begin
	  if(rd_en)
		  rd_data[(8*i)+7:(8*i)] <= ram [i] [rd_addr];
	end
end
endgenerate

integer di,dj;
initial begin
  for(di=0;di<`STRB_SIZE;di++) begin
    for(dj=0;dj<RAM_SIZE;dj++) begin
      ram[di][dj] = $urandom_range(8'h00, 8'hff);
    end
  end
end

endmodule

